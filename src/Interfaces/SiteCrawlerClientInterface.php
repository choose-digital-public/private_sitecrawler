<?php

namespace SiteCrawler\Interfaces;

interface SiteCrawlerClientInterface
{
    public function getStatusCode(string $domain);
}