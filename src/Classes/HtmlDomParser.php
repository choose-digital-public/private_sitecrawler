<?php

namespace SiteCrawler\Classes;

use simplehtmldom\HtmlWeb;
use SiteCrawler\Interfaces\SiteCrawlerParserInterface;

class HtmlDomParser implements SiteCrawlerParserInterface
{
    private HtmlWeb $htmlDom;
    private string $baseUrl;

    /**
     * HtmlDomParser constructor.
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->htmlDom = new HtmlWeb();
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param $url
     * @return \simplehtmldom\HtmlDocument|null
     */
    public function loadHtml($url): ?\simplehtmldom\HtmlDocument
    {
        return $this->htmlDom->load($url);
    }

    /**
     * @param $html
     * @param string $selector
     * @return mixed
     */
    public function findTagElement ($html, string $selector): mixed
    {
        return $html->find($selector);
    }
}