<?php

namespace SiteCrawler\Classes;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use SiteCrawler\Interfaces\SiteCrawlerClientInterface;

class SiteCrawlerClient implements SiteCrawlerClientInterface
{
    private Client $client;

    const HEADERS = ['User-Agent' => 'PostmanRuntime/7.36.0',
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip, deflate, br',
        'Connection'=> 'keep-alive'
    ];

    public function __construct(string $domain)
    {
        $this->client = new Client(['base_uri' => $domain, 'http_errors'=> false]);
    }

    /**
     * @param string $domain
     * @return string[]
     */
    public function getStatusCode(string $domain): array
    {
        $result = [
            'code' => '',
            'error' => ''
        ];

        try {
            $result['code'] = $this->client->request('GET', $domain, ['headers' => self::HEADERS])->getStatusCode();
        } catch (GuzzleException $e) {
            $result['code'] = 404;
            $result['error'] = 'Ссылка '.$domain.' недоступна по причине '.$e->getMessage();
        }

        return $result;
    }
}